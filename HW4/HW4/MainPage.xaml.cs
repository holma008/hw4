﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW4
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            PopulateListView();
        }

        public void PopulateListView()
        {
            var ListOfCharacters = new ObservableCollection<Character>();

            var Character1 = new Character();
            Character1.Picture = ImageSource.FromFile("Jim_Raynor.png");
            Character1.Name = "Jim Raynor";
            Character1.Title = "Marshal of Mar Sara";
            Character1.Url = "http://starcraft.wikia.com/wiki/Jim_Raynor";
            ListOfCharacters.Add(Character1);

            var Character2 = new Character();
            Character2.Picture = ImageSource.FromFile("Kerrigan.png");
            Character2.Name = "Sara Kerrigan";
            Character2.Title = "Queen of Blades";
            Character2.Url = "http://starcraft.wikia.com/wiki/Sarah_Kerrigan";
            ListOfCharacters.Add(Character2);

            var Character3 = new Character();
            Character3.Picture = ImageSource.FromFile("Zeratul.png");
            Character3.Name = "Zeratul";
            Character3.Title = "Dark Prelate";
            Character3.Url = "http://starcraft.wikia.com/wiki/Zeratul";
            ListOfCharacters.Add(Character3);

            var Character4 = new Character();
            Character4.Picture = ImageSource.FromFile("Artanis.png");
            Character4.Name = "Artanis";
            Character4.Title = "Hierarch of the Daelaam";
            Character4.Url = "http://starcraft.wikia.com/wiki/Artanis";
            ListOfCharacters.Add(Character4);

            var Character5 = new Character();
            Character5.Picture = ImageSource.FromFile("Arcturus_Mengsk.png");
            Character5.Name = "Arcturus Mengsk";
            Character5.Title = "Emperor of the Terran Dominion";
            Character5.Url = "http://starcraft.wikia.com/wiki/Arcturus_Mengsk";
            ListOfCharacters.Add(Character5);

            SC_Characters.ItemsSource = ListOfCharacters;
        }

        private void SC_Characters_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            Character itemTapped = (Character)listView.SelectedItem;
            var uri = new Uri(itemTapped.Url);
            Device.OpenUri(uri);
        }
    }
}
