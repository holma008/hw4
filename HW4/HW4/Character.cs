﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HW4
{
    public class Character
    {
        public ImageSource Picture
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public string Title
        {
            get;
            set;
        }
        public string Url
        {
            get;
            set;
        }
    }
}